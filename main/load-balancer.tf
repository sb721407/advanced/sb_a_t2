##### backend group #####

resource "yandex_alb_backend_group" "alb-bg" {
  name = "alb-bg"
  http_backend {
    name = "backend-1"
    port = 8080
    #    target_group_ids = [yandex_alb_target_group.alb-tg.id]
    target_group_ids = [yandex_compute_instance_group.ig-1.application_load_balancer.0.target_group_id]
    healthcheck {
      timeout          = "10s"
      interval         = "2s"
      healthcheck_port = 8080
      http_healthcheck {
        path = "/"
      }
    }
  }
}

##### http router Domain name akimov-petr.mooo.com #####

resource "yandex_alb_http_router" "alb-router" {
  name = "alb-router"
}

resource "yandex_alb_virtual_host" "alb-host" {
  name           = "alb-host"
  http_router_id = yandex_alb_http_router.alb-router.id
  authority      = ["alb1.akimov.space"]
  route {
    name = "route-1"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.alb-bg.id
      }
    }
  }
}

##### Application Load Balancer ######

resource "yandex_alb_load_balancer" "alb-1" {
  depends_on = [yandex_vpc_address.addr]
  name       = "alb-1"
  network_id = yandex_vpc_network.network-1.id
  #  security_group_ids = [yandex_vpc_security_group.alb-sg.id]
  security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id, yandex_vpc_security_group.alb-sg.id]
  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.subnet-1.id
    }

  }

  listener {
    name = "alb-listener"
    endpoint {
      address {
        external_ipv4_address {
          address = yandex_vpc_address.addr.external_ipv4_address[0].address
        }
      }
      ports = [8080]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.alb-router.id
      }
    }
  }
}
