resource "yandex_compute_instance" "monitoring" {
  name        = "monitoring"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
#      image_id = "fd87ap2ld09bjiotu5v0" # ubuntu-20
      image_id = "fd8emvfmfoaordspe1jr" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta2.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### ALB IP-Address and DNS A-record #####

resource "yandex_vpc_address" "addr" {
  name = "static-ip"
  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_dns_recordset" "server_dns_name" {
  depends_on = [yandex_vpc_address.addr]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "alb1"
  type       = "A"
  ttl        = 200
  data       = [yandex_vpc_address.addr.external_ipv4_address[0].address]
}

##### Monitoring DNS A-record #####

resource "yandex_dns_recordset" "monitoring_dns_name" {
  depends_on = [yandex_compute_instance.monitoring]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "mon1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}

##### Consul DNS A-record #####

resource "yandex_dns_recordset" "consul_dns_name" {
  depends_on = [yandex_compute_instance.monitoring]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "consul1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.monitoring.network_interface.0.ip_address]
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[monitoring]
${yandex_compute_instance.monitoring.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

EOF
  filename = "${path.module}/inventory"
}

##### Create file mydocker #####

resource "local_file" "mydockerservice" {
  content  = <<EOF

[Unit]
Description=Skillbox App Container Service
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
Restart=always
ExecStartPre=-/usr/bin/docker stop myapplication
ExecStartPre=-/usr/bin/docker rm myapplication
ExecStart=/usr/bin/docker run -p 8080:8080 --rm --name myapplication petrakimovdocker/skillboxapp:${var.COMMIT_TAG}
ExecStop=/usr/bin/docker stop -t 2 myapplication

[Install]
WantedBy=multi-user.target

EOF
  filename = "${path.module}/mydocker.service"
}

##### Provisioning #####

resource "null_resource" "monitoring" {
  depends_on = [yandex_compute_instance.monitoring, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
}
resource "null_resource" "server" {
  depends_on = [null_resource.monitoring]
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key -e 'committag=${var.COMMIT_TAG}' monitoring.yml"
  }
}

##### Provisioning2 #####

resource "null_resource" "git" {
  depends_on = [local_file.mydockerservice]
  provisioner "local-exec" {
    command = "./script.sh"
  }
}
