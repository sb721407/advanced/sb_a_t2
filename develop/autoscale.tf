##### Auto scale group #####

resource "yandex_compute_instance_group" "ig-1" {
  depends_on = [null_resource.server]
  name               = "autoscaled-ig"
  folder_id          = local.local_data.FOLDER_ID
  service_account_id = local.local_data.ACC_ID
  instance_template {
    platform_id = "standard-v2"
    resources {
      #      core_fraction = 5
      cores  = "2"
      memory = "2"
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
#        image_id = "fd87ap2ld09bjiotu5v0" # ubuntu-20
        image_id = "fd8emvfmfoaordspe1jr" # ubuntu-22
#        image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
        size     = 10 
      }
    }
    network_interface {
      network_id = yandex_vpc_network.network-1.id
      subnet_ids = ["${yandex_vpc_subnet.subnet-1.id}"]
      nat        = true
    }
    metadata = {
      user-data = "${file("${path.module}/meta.txt")}"
    }
    scheduling_policy {
      preemptible = true
    }
  }
  scale_policy {
    auto_scale {
      initial_size           = 2
      measurement_duration   = 120
      cpu_utilization_target = 10
      min_zone_size          = 2
      max_size               = 6
      warmup_duration        = 120
      stabilization_duration = 120
    }
  }
  allocation_policy {
    zones = ["ru-central1-a"]
  }
  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }
  application_load_balancer {
    target_group_name        = "target-group"
    target_group_description = "load balancer target group"
  }
}
