##### Variables #####

locals {
  local_data = jsondecode(file("${path.module}/config.json"))
}

##### General #####

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
  backend "http" {}
}

##### Variables from Gitlab-CI #####

variable "YANDEX_OAUTH" {
  type = string
}

variable "COMMIT_TAG" {
  type = string
}

##### Cloud #####

provider "yandex" {
  token = var.YANDEX_OAUTH
#  token = file(local.local_data.OAUTH_FILE)
  #  service_account_key_file = local.local_data.KEY_FILE
  cloud_id  = local.local_data.CLOUD_ID
  folder_id = local.local_data.FOLDER_ID
  zone      = local.local_data.ZONE
}

##### Net & Subnet definition #####

resource "yandex_vpc_network" "network-1" {
  name = "network-1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet-1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}
