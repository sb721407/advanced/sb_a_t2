#!/bin/sh
var1=$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
arr1=$( jo -a $var1 )
arg1=$( jo prometheus_retention_time=24h )
json=$( jo -p advertise_addr="$var1" telemetry="$arg1" bind_addr="$var1" bootstrap=false bootstrap_expect=1 client_addr="$var1 127.0.0.1" datacenter=dc1 retry_join="$arr1" server=true encrypt=86mYmtWxOA6fYjU+lM0pesQ3tl3FzEQIfVDJteu2EfU= encrypt_verify_incoming=true encrypt_verify_outgoing=true ui=true )
printf '%s\n' "$json" 
